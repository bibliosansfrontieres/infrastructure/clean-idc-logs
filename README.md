# clean-idc-logs

This script removes ungzipped files from the ansible logs directory on the
tincmaster server.

We assume that it s safe to remove ungzipped files when the gzipped version
exists. It means that on the remote device, logrotate already removed the
ungzipped file and rsync did upload the gzipped version.

## Cron

This script is inteneded to be ran by a monthly cronjob.

## Result

A two years span of accumulated duplicated logs worth ~2500 files / ~5GB.
